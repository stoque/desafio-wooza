import templateUrl from './plataformas.html';
import PlataformasService from './plataformas.service'

export const PlataformasComponent = {
  bindings: {
    plataformasData: '='
  },
  template: templateUrl,
  controller: class PlataformasComponent {
    constructor(PlataformasService, $state) {
      this.plataformasService = PlataformasService;
      this.state = $state;
    }
    selectPlataforma(sku) {
      this.state.go('planos', {sku: sku});
    }
  }
};