export default class PlataformasService {
  constructor($http) {
    this.$http = $http
  }

  getPlataformas() {
    return this.$http.get('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas')
      .then(response => response.data);
  }
}