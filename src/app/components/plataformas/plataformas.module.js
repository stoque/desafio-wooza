import angular from 'angular';

import { PlataformasComponent } from './plataformas.component'
import PlataformasService from './plataformas.service'

import './plataformas.scss';

export default angular
  .module('plataformas', [
  ])
  .component('plataformas', PlataformasComponent)
  .service('PlataformasService', PlataformasService)
  .config(($stateProvider) => {
    $stateProvider
    .state('plataformas', {
      url: '/',
      component: 'plataformas',
      resolve: {
        plataformasData: PlataformasService => PlataformasService.getPlataformas()
      }
    })
  })
  .name
