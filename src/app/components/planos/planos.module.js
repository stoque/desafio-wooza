import angular from 'angular';

import { PlanosComponent } from './planos.component'
import Plataformas from '../plataformas/plataformas.module'
import PlanosService from './planos.service'

import './planos.scss';

export default angular
  .module('planos', [
    Plataformas
  ])
  .component('planos', PlanosComponent)
  .service('PlanosService', PlanosService)
  .config($stateProvider => {
    $stateProvider
    .state('planos', {
      url: '/planos/:sku',
      component: 'planos',
      resolve: {
        planosData: (PlanosService, $stateParams) => PlanosService.getPlanos($stateParams.sku)
      }
    })
  })  
  .name