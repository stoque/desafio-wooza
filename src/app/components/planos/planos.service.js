export default class PlanosService {
  constructor($http) {
    this.$http = $http
  }

  getPlanos(sku) {
    return this.$http.get('http://private-59658d-celulardireto2017.apiary-mock.com/planos/' + sku).then(response => response.data)
  }
}