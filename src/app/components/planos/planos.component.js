import templateUrl from './planos.html';

export const PlanosComponent = {
  bindings: {
    planosData: '='
  },
  template: templateUrl,
  controller: class PlanosComponent {
    constructor($state, $stateParams) {
      this.state = $state;
      this.skuPlataforma = $stateParams.sku;
    }
    selectPlano(sku) {
      this.state.go('form', { 
        skuPlataforma: this.skuPlataforma, 
        skuPlano: sku 
      })
    }
  }
};