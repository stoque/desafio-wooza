import angular from 'angular';
import inputMasks from 'angular-input-masks'

import { FormComponent } from './form.component';

import './form.scss';

export default angular
  .module('form', [
    inputMasks
  ])
  .component('formWooza', FormComponent)
  .config($stateProvider => {
    $stateProvider
    .state('form', {
      url: '/form/:skuPlataforma/:skuPlano',
      component: 'formWooza',
    })
  })  
  .name