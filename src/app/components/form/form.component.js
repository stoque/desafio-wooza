import templateUrl from './form.html';

export const FormComponent = {
  template: templateUrl,
  controller: class FormComponent {
    constructor($stateParams, $scope) {
      this.skus = {
        plataformaSku: $stateParams.skuPlataforma,
        planoSku: $stateParams.skuPlano
      };

      this.$scope = $scope;

      this.successMessage = false;
    }
    sendData(info) {
      let data = Object.assign(info, this.skus);
      console.log(data);
      delete this.$scope.info;
      this.successMessage = true;
    }
  }
} 