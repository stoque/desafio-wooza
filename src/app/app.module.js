import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import { AppComponent } from './app.component';

import Planos from './components/planos/planos.module';
import Plataformas from './components/plataformas/plataformas.module';
import Form from './components/form/form.module';

import './app.scss';

angular
  .module('wooza', [
    uiRouter,
    Plataformas,
    Planos,
    Form
  ])
  .component('app', AppComponent)
  .config($urlRouterProvider => $urlRouterProvider.otherwise('/'))
  .name