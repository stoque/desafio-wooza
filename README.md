# Desafio Front-End Wooza

[Acesse o projeto em produção aqui](http://desafio-wooza.surge.sh)

### Instalação

```
# Clone o repositório
$ git clone https://bitbucket.org/stoque/desafio-wooza

# Entre no diretório do projeto
$ cd desafio-wooza

# Instale as dependências
$ npm install

# Suba o ambiente de desenvolvimento
$ npm run start
```
