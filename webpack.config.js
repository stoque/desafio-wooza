const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: './src/app/app.module.js',
  output: {
      path: path.resolve(__dirname, 'dist/js'),
      filename: 'bundle.js',
  },
  module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        },
        {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader'
          })
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract({
            use: 
            [{
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader'
            }],
            fallback: 'style-loader'
          })
        },
        { test: /\.html$/, loader: "html-loader" },
        { test: /\.css$/, loader: "style!css" },
    ]
  },
  plugins: [
    new ExtractTextPlugin('../css/styles.css')
  ]
}